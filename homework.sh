#!/bin/bash

output_dir="exercise_folder"

usage() {
    echo "Usage: $0 -f input_file [-o output_dir]"
    exit 1
}

while getopts "f:o:" opt; do
    case ${opt} in
        f)
            input_file=${OPTARG}
            ;;
        o)
            output_dir=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
# Check we have our input file or no
if [ -z "${input_file}" ]; then
    usage
fi

host_vars_dir="${output_dir}/host_vars"
mkdir -p "${host_vars_dir}"

hosts_file="${output_dir}/hosts"
echo "[all]" > "${hosts_file}"
# IDK, but if we use sed after 'done' with "<" sed didn't work
# Here sed delete removing extra spaces up to the next character
sed 's/[[:space:]]//g' "${input_file}" | while IFS='|' read -r _ hostname ip_address host_type comment; do
    
    if [[ "$hostname" != "Hostname" && -n "$hostname" && ("$hostname" != ":---" && "$hostname" != "---:" && "$hostname" != "---" ) ]]; then
        echo "$hostname" >> "${hosts_file}"
        echo "ansible_host: ${ip_address}" > "${host_vars_dir}/${hostname}"
    fi
done
