# Instruction for running script

## List of the main files:

### `file.txt`
- **There is a text file with the following format:**
```
# SIT-SVT-trunc internal environment

## Environment VMs

| Hostname       | Floating IP     | Host type          | Comment          |
| :---           | ---:            | ---:               | ---              |
| tmapp01        | 10.122.122.189  | toms_app           | TOMS Application |
| tmapp02        | 10.122.122.190  | toms_app           | TOMS Application |
| tmodb01        | 10.122.122.191  | toms_odb           | TOMS ODB         |
| tbcdb01        | 10.122.122.168  | tbapi_couchbase    | TBAPI Couchbase  |
| tbcdb02        | 10.122.122.177  | tbapi_couchbase    | TBAPI Couchbase  |
| tbcdb03        | 10.122.122.178  | tbapi_couchbase    | TBAPI Couchbase  |
```


### `homework.sh`
- **script to convert that file in the following structure:**
```
📂exercise_folder/
┣ 📂host_vars/
┃  ┣ 📜tmapp01
┃  ┣ 📜tmapp02
┃  ┣ 📜tmodb01
┃  ┣ 📜tbcdb01
┃  ┣ 📜tbcdb02
┃  ┗ 📜tbcdb03
┗📜hosts
```
where
    tbcdb01 is a file with the following format:

```
ansible_host:  10.122.122.168 
```

    and hosts is a common file to list all the hosts like:

```
[all]
tmapp01
tmapp02
tmodb01
tbcdb01
tbcdb02
tbcdb03
```

## How to run script

### 1. Download git repo to local
```
    git clone https://gitlab.com/Esimseitm/01-bash.git
```
### 2. Switch from `main` branch to `master` branch
```
    git checkout master
```
### 3. Give permissions to script execute(if dont have permissons)
```
    chmod +x homework.sh
```
### 4. Run script option-1(default directory)
```
    ./homework.sh -f file.txt
```
### 5. Run script option-2(choose the output directory)
```
    ./homework.sh -f file.txt -o directory_choosen
```


